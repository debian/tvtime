News for 1.0.11
  * Using tvtime over ssh now works without setting an environment var
  * Fix crash when no v4l capture devices are found
  * Fix rdtsc on x86_64
  * Fix asm code in cpuid() clobering the upp

News for 1.0.10
  * No code changes
  * Added translations for Ukranian and Slovak
  * Updated pt and pt_BR translations

News for 1.0.9
  * Added yuv planar formats to work with Wayland
  * Fixed compilation issues with gcc6
  * Improved audio/mixer and ALSA streaming logic
  * Added logic to autodetect the associated ALSA device
  * Make it adehent with newer freedesktop.org standards
  * Added Catalan translation
  * pt-BR and es translation updated

News for 1.0.8
  * Change translation man pages and messages to use UTF-8

News for 1.0.7

  * Fix warning about Makefiles ignoring the --datarootdir parameter
  * Remove unused functions
  * Safely assume C89 semantics and make RETSIGTYPE void
  * Replace deprecated function XKeycodeToKeysym with XkbKeycodeToKeysym
  * Add gitignore to plugins directory
  * Link to libsupc++ instead of bringing in libstdc++.
  * Actually call function
  * Set subdir-objects automake option
  * Revert "Set subdir-objects option for automake"
  * Set automake flavour to foreign
  * Set subdir-objects option for automake
  * Revert accidental replacement of INSTALL file
  * Turn automake silent by default
  * Fix bashisms in boostrap
  * Remove unused safetytime variable
  * Remove embedded Debian packaging directory

News for 1.0.6

  * Change alsa mixer from default/Line to default/Master
  * Fix the mixer error message when channel is not available

News for 1.0.5

  * Fix tvtime-scanner crash with home unset
  * Fix input select condition
  * pt_BR: Update a few Brazilian Portuguese translations
  * mkinstalldirs: update to work with newer versions of autoconf
  * Don't block indefinitely waiting for frames
  * Accept arbritary height resolution
  * Use videoinput_get_norm_height() to detect 60Hz based standards
  * Don't require VIDIOC_*_INPUT support
  * Improve error message about invalid framesize
  * Add proper credits to Debian package and Philipp Hahn
  * Add an option to disable the borders
  * Fix two warnings when autoreconf is called
  * Fix a few issues requested by libtool stuff

News for 1.0.4

  * Fix portuguese translation.
  * Add a parameter to enable borderless mode.
  * Fix XScreenSaver extension check.
  * Fix the invisible cursor not being invisible due to using uninitialized
     memory.
  * Fix setting the window icon on 64-bit systems.
  * Some updates to tvtime.desktop, see gentoo bug #308297
  * Initialize coords when going fullscreen, see redhat bug #235622.
  * Remove unused and obsolete metacity check.
  * Add latvian translation.
  * Import translation updates for french, portuguese and russian
    from the debian package.
  * Change right mouse button while in menu to leave menu.
  * Leave volume muted when decreasing volume, this way if the volume
    is accidentally way too loud, you can mute and decrease without it
    coming back on.
  * Don't try to implement focus follows mouse mode for just our
      window, that's up to the window manager.
  * Fix check for PIC define in cpuaccel, see gentoo bug #74227
  * Import patch from gentoo to make xinerama optional at configure
    time.
  * Remove the bundled videodev*.h headers.
  * Apply patch from #1398391 Set the memory field when calling
    VIDIOC_DQBUF ioctl.
  * Fix crash on "tvtime-command set_input_width 720", patch from
    sf tracker.
  * Add zlib.h include since libpng doesn't bring it in in 1.5.
  * Rename struct typedef from reserved locale_t to xmltv_locale.
  * Import C++ fixes for tomsmocomp so it compiles with gcc 4.1+.
  * Fix documentation to say MENU_ENTER/BACK instead of RIGHT/LEFT.
  * Import some formatting fixes for the manpages from the debian
    package.

News for 1.0.3
  * V4L1 removal
  * Alsa streaming support
  * Compilation fixes, patch backports from other places
  * More generic VBI handling

News for 1.0.2

  *  Add back the missing translations from the 1.0.1 release.
  *  Allow key bindings to map to noop to disable keys.

News for 1.0.1

  *  Minor updates that were intended for the 1.0 release.

News for 1.0

  *  The cx88 driver cannot detect the PAL audio mode (BG/DK/I),
     and attempts to reverse engineer the Windows driver have failed.
     tvtime now allows users to select between BG/DK/I mode in the
     UI to help work around this driver limitation.
  *  By default, tvtime now assumes that pixels are square.  This
     avoids problems with many misconfigured X servers.

News for 0.99

  *  Test release for 1.0.
  *  Major bugfixes and cleanup.

News for 0.9.15

  *  Compile fixes.

News for 0.9.14

  *  Improved the usability of the menu system when using a mouse.
  *  Fixed some problems with Xinerama and fullscreen.
  *  Streamlined the default set of key bindings.
  *  Show an icon in modern window list applications.
  *  Fixed colour problems for DVB driver users.
  *  Changed the default process priority to -10 instead of -19.
  *  Fixed compile problems with new gcc versions.

News for 0.9.13

  *  Added support for changing the default language read from XMLTV.
  *  Added support for PAL-BG/DK audio mode switching.
  *  Added an audio boost setting for controlling capture card volume.
  *  Fixed a bunch of critical bugs.
  *  Removed the SDL, MGA, and DirectFB output drivers.
  *  Removed the unfinished MPEG2 support and the unreleased recording
     application 'rvr'.

News for 0.9.12

  *  Minor bugfixes from 0.9.11.

News for 0.9.11

  *  Fixed gettext to initialize properly.
  *  Improved our XMLTV on-screen-display.
  *  Display the upcoming show name from XMLTV.
  *  Changed fifodir back to /tmp.
  *  Lots of documentation improvements.
  *  Fixed lots of bugs.

News for 0.9.10

  *  Added program listings using XMLTV.  Use with --xmltv=filename.
  *  Added commands for chroma kill, sleep timer, and enabling or
     disabling signal detection.
  *  Added the matte mode setting to the menu.
  *  Added a DirectFB output layer by Ville Syrjala.
  *  Significantly lowered the size of the tvtime helper executables.
  *  Various menu system cleanups.
  *  Fixed a ton of bugs.

News for 0.9.9

  *  Added a full menu system for configuring tvtime.
  *  Simplified deinterlacer names and provide online help for them.
  *  Unified OSD appearance across all television standards.
  *  Support for saving picture settings both globally and per-channel.
  *  Finetune settings saved per-channel.
  *  Support for the V4L2 DVB driver and bttv9 for PAL-60 support.
  *  Added a channel scanner application 'tvtime-scanner' that outputs
     to the 'Custom' frequency table.
  *  Native support for UYVY input from the rivatv driver.
  *  Removed lirc support in favour of using tvtime-command.
  *  Fixed remaining screensaver, Xinerama and fullscreen issues.
  *  Many other new features and bugfixes.

News for 0.9.8.5

  *  Some root safety improvements.
  *  Added support for long-format command line arguments.
  *  Added config file options to set startup window position.
  *  Compile fixes for non-C99 compilers and older DirectFB versions.

News for 0.9.8.4

  *  Fixed to work with metacity in RH 8.0.

News for 0.9.8.3

  *  Support for norm switching between "compatible" norms for areas
     which receive both SECAM and PAL format stations.
  *  Added support for the mga_vid driver for the Matrox G200/G400
     series cards from mplayer.
  *  Semi-experimental of the X code for better fullscreen support under
     many window managers.  Testing appreciated.
  *  Support for changing the mixer device and channel (see 'man tvtime'
     and the config file documentation).
  *  Slightly less partial experimental UTF-8 support. Now works with
     any simple left-to-right scripts which are in FreeSansBold.ttf .

News for 0.9.8.2

  *  Fixed to compile on older gcc versions (blame pv2b).

News for 0.9.8.1

  *  Alot of minor bugfixes.
  *  Partial EXPERIMENTAL UTF-8 support. Only works with UTF-8
     characters that are also in ISO-8859-1 (Latin-1).

News for 0.9.8

  Users of tvtime 0.9.7:
    The default key bindings and configuration file format have
    significantly changed in this release of tvtime.  Please see the
    documentation for details on using tvtime 0.9.8.

    Also, some command line options have changed or become unavailable
    (no more -w, use -H instead).  Please consult the tvtime man page.

  *  New XML config file format for both the tvtime configuration,
     and the station list.
  *  tvtime now has two distinct half-framerate modes: top-field-first
     frames, and bottom-field-first frames.  This helps with manually
     deinterlacing 2-2 pulldown or progressive content.
  *  Added support for preset 'modes', which can set the deinterlacer,
     window size, fullscreen setting and framerate mode.
  *  Added support for custom frequencies in the stationlist.xml file,
     and a runtime command to add a new station from the current
     fine-tuning settings.
  *  Added some DScaler DLLs and a wine layer to load these deinterlacer
     plugins directly without porting to Linux.  This gives us the
     following new deinterlacers:
       GreedyH        DScaler: Greedy - High Motion
       Greedy2Frame   DScaler: Greedy - 2-frame
       TwoFrame       DScaler: TwoFrame
       TomsMoComp     DScaler: TomsMoComp
       VideoBob       DScaler: Video Bob
       VideoWeave     DScaler: Video Weave
     We had our own ports of videobob, twoframe, and greedy2frame,
     but they were found to be buggy.  Our ports are currently disabled.
  *  Added the ffmpeg vertical deinterlacer filter.
  *  Support for multiple audio channels (stereo, mono, lang1, lang2).
  *  Included a bunch of fixes for PAL-M support.
  *  Added a slave mode for freevo integration.
  *  Various speedups, quality improvements, and bugfixes.

News for 0.9.7

  *  Moved config files into a new config file directory ~/.tvtime
  *  Reworked the frequency tables, merging europe-cable, europe-west,
     europe-east all into 'europe'.  Added the 'russia' frequency table.
  *  Added a channel scanner.  F10 walks through the channel list and
     disables inactive channels.
  *  Added the 'XVideo Bob' deinterlacer filter.
  *  Added lirc support.
  *  Added the overscan feature.
  *  Added hardware support webpage.
  *  Added a half-framerate mode ('=' to activate it).
  *  Fixed alot of bug reports.

News for 0.9.6

  *  We updated the tuner frequencies, please make sure you're up to
     date with our naming.
  *  We added alot of keymaps, so we also changed some of the default
     keys!
  *  tvtime now shows a blue screen if the tuner reports no signal.
     Please let me know if this exposes any problems.

